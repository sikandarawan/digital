# Tasks

I work on task 2 and 4.

## Task 2
I Worked on distanceFeed() where we updating data with all fields which are not even send in request
we should only update those fields which are we receiving or make them required.
One more thing which I noticed
why we not make jobid required even when our update model depend on jobid.
We are using custom return check when we can handle it with laravel request
In my opinion we should make seperate request when we need to validate request.




## Task 4

Logic is not good in this function we were using
```php
if ($difference <= 90)
```
Its mean our call difference which is below 90 will execute inner code of this
condition and other conditions like below never true in any case
```php
if ($difference > 24 && $difference <= 72)
and 
if ($difference <= 24)
```

## About Code
I have some suggestions which are mention below.

- Some functions length is too long which increase its complexity.
- Its better we add a  service layer for business logic rather than in controller.
- Some places first we insert than again update even where we can handle it in single query.
- Unused declared variable should remove.
- Should follow same standard on all response eg in one place we use

```php response('Record updated!') ```

  and on other place

  ```php response(['success' => 'SMS sent']```
  We should follow same pattern and its better if make resource collection.
  
